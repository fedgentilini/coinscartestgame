﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockManager : MonoBehaviour
{
    private float coinChanceValue;
    public float coinChance
    {
        get
        {
            return coinChanceValue;
        }
        set
        {
            coinChanceValue = value;
            if (value>1)
            {
                coinChanceValue = 1;
            }
        }
    }

    private float oilChanceValue;

    public float oilChance
    {
        get
        {
            return oilChanceValue;
        }
        set
        {
            oilChanceValue = value;
            if (value +coinChance > 1)
            {
                oilChanceValue = 1-coinChance;
            }
        }
    }

    float verticalOffset=1.8f;
    float horizontalOffset=1.5f;

    Stack<GameObject> coins=new Stack<GameObject>();
    Stack<GameObject> oils = new Stack<GameObject>();
    List<GameObject> usedCoins = new List<GameObject>();
    List<GameObject> usedOils = new List<GameObject>();

    public Transform baseSpawningTransform;
    public Vector2 baseSpawningPosition;
    public GameObject coin;
    public GameObject oil;

    // Start is called before the first frame update
    void Start()
    {
        baseSpawningPosition= baseSpawningTransform.position;
    }

    public void MoveDown(float moveAmount)
    {
        transform.position += Vector3.down * moveAmount;
    }

    public void RandomizeNewBlockContent()
    {
        ClearBlock();

        Vector3 newPosition=Vector3.zero;
        for (int i=0; i<7;i++)
        {
            int oilCount = 0;
            for (int j = 0; j < 3; j++)
            {
                
                newPosition = new Vector3( j * horizontalOffset,  i*verticalOffset,-0.1f);
                float chance=Random.Range(0,1f);
                if (chance < coinChance)
                {
                    AddCoin(newPosition);
                }
                else if (chance < coinChance+oilChance)
                {
                    if (oilCount<2 && i%3==1)
                    {
                        oilCount++;
                        AddOil(newPosition);
                    }
                }
            }
        }
    }

    public void ClearBlock()
    {
        foreach (GameObject c in usedCoins)
        {
            c.SetActive(false);
            coins.Push(c);
        }
        usedCoins.Clear();
        foreach (GameObject o in usedOils)
        {
            o.SetActive(false);
            oils.Push(o);
        }
        usedOils.Clear();

    }

    void AddCoin(Vector3 localPosition)
    {
        if (coins.Count == 0)
        {
            GameObject c=Instantiate(coin,localPosition,Quaternion.identity, baseSpawningTransform);
            c.transform.localPosition = localPosition;
            usedCoins.Add(c);
        }
        else
        {
            GameObject c = coins.Pop();
            c.SetActive(true);
            c.transform.localPosition =localPosition;
            usedCoins.Add(c);
        }
    }

    void AddOil(Vector3 localPosition)
    {
        if (oils.Count == 0)
        {
            GameObject o = Instantiate(oil, localPosition, Quaternion.identity, baseSpawningTransform);
            o.transform.localPosition = localPosition;
            usedOils.Add(o);
        }
        else
        {
            GameObject o = oils.Pop();
            o.SetActive(true);
            o.transform.localPosition = localPosition;
            usedOils.Add(o);
        }
    }
}

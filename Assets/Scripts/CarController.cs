﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public delegate void CollectibleTouchedDelegate();
    public event CollectibleTouchedDelegate OnCoinTouched;
    public event CollectibleTouchedDelegate OnOilTouched;

    float maxRotation;
    public float steeringSpeed;
    public float adjustmentSpeed;    
    Rigidbody2D carRigidBody;
    float carSpeed;
    bool isSteeringRight = false;
    bool isSteeringLeft = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            collision.gameObject.SetActive(false);
            OnCoinTouched();
        }

        if (collision.CompareTag("Oil"))
        {
            OnOilTouched();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        carRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HandleCarRotation();
        
        // Horizontal movement
        carRigidBody.MovePosition((Vector2)transform.position - (new Vector2(carSpeed*2.5f, 0)*Mathf.Sin(transform.eulerAngles.z*Mathf.Deg2Rad)*Time.fixedDeltaTime));
    }

    private void HandleCarRotation()
    {
        if (isSteeringLeft)
        {
            SteerLeft();
        }
        else if (isSteeringRight)
        {
            SteerRight();
        }
        else //if I'm not steering reallign the car to the road
        {
            float angle = transform.eulerAngles.z;
            if (angle > 180)
            {
                angle = angle - 360;
            }

            if (angle < -0.5f)
            {
                carRigidBody.MoveRotation(transform.eulerAngles.z + (adjustmentSpeed * Time.deltaTime));

            }
            else if (angle > 0.5f)
            {

                carRigidBody.MoveRotation(transform.eulerAngles.z - (adjustmentSpeed * Time.deltaTime));
            }

        }
    }

    public void SteerLeft()
    {

        float angle = transform.eulerAngles.z;
        if (angle > 180)
        {
            angle = angle - 360;
        }

        if (angle < 25f)
        {
            carRigidBody.MoveRotation(transform.eulerAngles.z + (steeringSpeed * Time.deltaTime));
        }
        else
        {
            carRigidBody.rotation = 25;
        }
    }

    public void SteerRight()
    {
        float angle = transform.eulerAngles.z;
        if (angle > 180)
        {
            angle = angle - 360;
        }

        if (angle > -25f)
        {
            carRigidBody.MoveRotation(transform.eulerAngles.z - (steeringSpeed * Time.deltaTime));
        }
        else
        {
            carRigidBody.rotation = 335;
        }
    }

    public void SetSteeringRight(bool steeringMode)
    {
        if (!steeringMode)
        {
            isSteeringRight = false;
        }
        else
        {
            isSteeringLeft = false;
            isSteeringRight = steeringMode;
        }
    }
    public void SetSteeringLeft(bool steeringMode)
    {
        if (!steeringMode)
        {
            isSteeringLeft = false;
        }
        else
        {
            isSteeringRight = false;
            isSteeringLeft = steeringMode;
        }
    }

    public void SetSpeed(float speed)
    {
        carSpeed = speed;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    AsyncOperation loadSceneAsyncOperation;
    public CarController car;
    private Queue<RoadBlockManager> roadBlocksQueue;
    public RoadBlockManager[] roadBlocks;
    public GameObject endGamePanel;
    public Text scoreText;
    public Text finalScoreText;

    public float coinChance;
    public float oilChance;
    private bool isGameOver;

    public float lowerBoundY;
    public float roadBlockLength;
    public float startingSpeed;
    public float slowDownAcceleration;
    private float carSpeedValue;
    public float carSpeed
    {
        get
        {
            return carSpeedValue;
        }

        set
        {
            carSpeedValue = value;
            car.SetSpeed(value);
        }
    }
    public int scoreValue = 0;

    public int score
    {
        get
        {
            return scoreValue;
        }

        set
        {
            scoreValue = value;
            scoreText.text = "" + value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        carSpeed = startingSpeed;
        roadBlocksQueue = new Queue<RoadBlockManager>(roadBlocks);
        car.OnCoinTouched += IncreaseScore;
        car.OnOilTouched += GameOver;
        foreach (RoadBlockManager r in roadBlocksQueue)
        {
            r.coinChance=coinChance;
            r.oilChance = oilChance;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ScrollRoad();

        if (!isGameOver)
        {
            HandleInput();
        }
    }

    private void HandleInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            car.SetSteeringLeft(true);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            car.SetSteeringRight(true);
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A))
            {
                car.SetSteeringLeft(false);
            }
            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D))
            {
                car.SetSteeringRight(false);
            }
        }
    }

    private void ScrollRoad()
    {
        foreach (RoadBlockManager r in roadBlocksQueue)
        {
            r.MoveDown(carSpeed * Time.deltaTime);
        }

        RoadBlockManager bottomRoadBlock= roadBlocksQueue.Peek();
        if (bottomRoadBlock.transform.position.y < lowerBoundY)
        {
            //move the bottom block to the top
            bottomRoadBlock.transform.position += new Vector3(0, roadBlockLength * 3, 0);

            bottomRoadBlock.RandomizeNewBlockContent();
            roadBlocksQueue.Enqueue(roadBlocksQueue.Dequeue());
        }
    }

    public void IncreaseScore()
    {
        if (!isGameOver)
        {
            score++;
        }
    }

    public void GameOver()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            StartCoroutine(EndGameSlowDownRoutine());
            Debug.Log("GameOver dude!");
        }
    }

    public IEnumerator EndGameSlowDownRoutine()
    {     
        loadSceneAsyncOperation = SceneManager.LoadSceneAsync(0);
        loadSceneAsyncOperation.allowSceneActivation = false;
        while (carSpeed>0)
        {
            carSpeed -= slowDownAcceleration * Time.deltaTime;
            if (carSpeed < 0)
            {
                carSpeed = 0;
            }
            yield return null;
        }
        endGamePanel.SetActive(true);
        finalScoreText.text = ""+score;
    }

    public void Restart()
    {
        loadSceneAsyncOperation.allowSceneActivation = true;
    }
   
}
